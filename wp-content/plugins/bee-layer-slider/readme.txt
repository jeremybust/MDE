=== Slider ===
Contributors: aumsrini
Donate link: http://beescripts.com/donate
Tags: slider,layer slider, responsive image slider ,gallery slider,banner slider,text slider,wp slider
Requires at least: 3.0.1
Tested up to: 4.6.1
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

slider slideshow is a layer slider with multi text layer,background option for each slide and animations for slider .

== Description ==

Slider Slideshow is a layer slider plugin to create responsive slider with image and text layer.

Layer Slider Slideshow plugin for WordPress.It is a responsive image and text layer slider plugin. Layer Slider Slideshow plugin have admin settings for image and text layers.layer Slider slideshow plugin is fully responsive to create custom slideshows with Image and texts. Layer Slider Slideshow plugin have built in animation effects for image and text layers.Layer slider slideshow have background options for each slide.Layer slider slide show plugin allows you to create unlimited sliders.

Wordpress layer slideshow plugin is easy way to create your site slides in minutes without any html or coding knowledge.You can create unlimited sliders.Layer slider slideshow allows you to use it on any wordpress projects without any restriction.This Slider Plugin supports any device.Aslo we provide free support.

**Basic Slider Features**

1. Responsive
1. Single Image Layer
1. Multiple Text Layer
1. Background Image option for each slide
1. Create Unlimited slides
1. Text custom settings in admin
1. Image LayerCustom Settings in admin
1. 9 Animaton Effects


**Watch Below Video For Demo**
[vimeo https://vimeo.com/186046757]

For support please feel free to contact here  [Beescripts](http://beescripts.com/contact "Your favorite software") 

Click here [Upgrade To Pro](http://beescripts.com/product/layer-slider "Bee Layer Slider Pro")

**Pro Version Features**


1. Unlimited Image Layer
1. Text layer  settings for each layer.
1. Image layer settings for each slider
1. Easing effect for Image layer
1. Easing effect for text layer
1. Individual animation for each image and text layer
1. Custom css
1. 1 year support and updates
1. Use it on unlimited domains

**Watch Below Video For Pro Version**
[vimeo https://vimeo.com/187946664]


== Installation ==

1. Upload `bee-slider.zip` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Place [bee-slider id=your slider id] in your page or post
1. Place `<?php echo do_shortcode("[bee-slider id=your slider id]"); ?>` in your templates

== Frequently Asked Questions ==

= Is your Plugin Responsive ? =

Yes,this slider plugin is responsive.

= Is there any settings for slider ? =

Yes,There are general,text layer and image layer settings are in admin section.

== Screenshots ==
1. Front View
2. Admin slider pages
3. Admin Slider Options

== Changelog ==

= 1.1 =
* admin js bug fixed.

= 1.0 =
* Initial release

== Upgrade Notice ==

= 1.1 =
Admin Javascript bugs fixed.Kindly update.This more important to work on back end
